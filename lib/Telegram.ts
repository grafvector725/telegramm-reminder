import { default as Telegraf, ContextMessageUpdate } from "telegraf";
import { TOKEN, socksConfig } from "./utils/Consts";
// tslint:disable:variable-name
const Agent = require('socks5-https-client/lib/Agent');

export class Telegram {
  private telegraf;
  static  socksAgent = new Agent({
    socksHost: socksConfig.host,
    socksPort: socksConfig.port,
    socksUsername: socksConfig.login,
    socksPassword: socksConfig.psswd,
  });

  constructor() {
    this.telegraf = new Telegraf(TOKEN);
  }

  private setConfig() {}

  public getTelegrafManager() {
    return this.telegraf;
  }
}
